/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.ddl2pojo;

import com.helger.jcodemodel.JAnnotationArrayMember;
import com.helger.jcodemodel.JAnnotationUse;
import com.helger.jcodemodel.JCodeModel;
import com.helger.jcodemodel.JCodeModelException;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JMod;
import com.helger.jcodemodel.JPackage;
import com.helger.jcodemodel.writer.JCMWriter;
import com.voxelbuster.ddl2pojo.generators.JEqualsMethod;
import com.voxelbuster.ddl2pojo.generators.JGetter;
import com.voxelbuster.ddl2pojo.generators.JHashCodeMethod;
import com.voxelbuster.ddl2pojo.generators.JSetter;
import com.voxelbuster.ddl2pojo.generators.JToStringMethod;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.Statements;
import net.sf.jsqlparser.statement.alter.Alter;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.UniqueConstraint;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.voxelbuster.ddl2pojo.D2PUtils.capitalize;
import static com.voxelbuster.ddl2pojo.D2PUtils.convertCase;
import static com.voxelbuster.ddl2pojo.D2PUtils.uncapitalize;

@Slf4j
@Mojo(name = "ddl2pojo", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class DDL2PojoMojo extends AbstractMojo {

    @SuppressWarnings("unused")
    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    private MavenProject project;

    /**
     * Project paths to search for SQL DDL files.
     */
    @SuppressWarnings("unused")
    @Parameter(property = "ddlSearchPaths")
    private List<String> ddlSearchPaths;

    /**
     * Regex to match file names to include in the SQL DDL search.
     */
    @SuppressWarnings({"unused"})
    @Parameter(property = "fileFilters")
    private List<String> fileFilters;

    /**
     * Destination for generated source files.
     */
    @SuppressWarnings("unused")
    @Parameter(property = "generatedSourcesDest", defaultValue = "${project.baseDir}/src/generated/java")
    private String generatedSourcesDest;

    /**
     * Destination package for generated entity classes.
     */
    @SuppressWarnings("unused")
    @Parameter(property = "targetEntityPackage")
    private String targetEntityPackage;

    /**
     * Destination package for generated DTO classes.
     */
    @SuppressWarnings("unused")
    @Parameter(property = "targetDtoPackage")
    private String targetDtoPackage;

    /**
     * Boolean to set whether to generate entity classes.
     */
    @SuppressWarnings("unused")
    @Parameter(property = "generateEntities", defaultValue = "true")
    private boolean generateEntities;

    /**
     * Boolean to set whether to generate DTO classes.
     */
    @SuppressWarnings("unused")
    @Parameter(property = "generateDtos", defaultValue = "true")
    private boolean generateDtos;

    /**
     * Boolean to set whether to separate packages by schema names.
     */
    @SuppressWarnings("unused")
    @Parameter(property = "separatePackageBySchama", defaultValue = "true")
    private boolean separatePackageBySchema;

    /**
     * Boolean to set whether the process should fail when there is an issue with the code generation pipeline
     * (such as a nonexistent directory).
     */
    @SuppressWarnings("unused")
    @Parameter(property = "failOnError", defaultValue = "false")
    private boolean failOnError;

    /**
     * Boolean to set whether to generate classes for tables.
     */
    @SuppressWarnings("unused")
    @Parameter(property = "includeTables", defaultValue = "true")
    private boolean includeTables;

    /**
     * Boolean to set whether to generate classes for views.
     */
    @SuppressWarnings("unused")
    @Parameter(property = "includeViews", defaultValue = "true")
    private boolean includeViews;

    /**
     * A list of Java annotations to apply to all entity classes.
     * Use full annotation syntax, for example "<code>@Getter(value = AccessLevel.PACKAGE)</code>".
     */
    @SuppressWarnings("unused")
    @Parameter(property = "entityTypeAnnotations")
    private List<String> entityTypeAnnotations;

    /**
     * A list of Java annotations to apply to all DTO classes.
     * Use full annotation syntax, for example "<code>@Getter(value = AccessLevel.PACKAGE)</code>".
     */
    @SuppressWarnings("unused")
    @Parameter(property = "dtoTypeAnnotations")
    private List<String> dtoTypeAnnotations;

    /**
     * Prefix for all entity types.
     */
    @SuppressWarnings("unused")
    @Parameter(property = "entityTypePrefix")
    private String entityTypePrefix;

    /**
     * Suffix for all entity types.
     */
    @SuppressWarnings("unused")
    @Parameter(property = "entityTypeSuffix", defaultValue = "Entity")
    private String entityTypeSuffix;

    /**
     * Prefix for all DTO types.
     */
    @SuppressWarnings("unused")
    @Parameter(property = "dtoTypePrefix")
    private String dtoTypePrefix;

    /**
     * Suffix for all DTO types.
     */
    @SuppressWarnings("unused")
    @Parameter(property = "dtoTypeSuffix", defaultValue = "EntityDto")
    private String dtoTypeSuffix;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        Path dtoSrcDir = Paths.get(generatedSourcesDest, targetDtoPackage.split("\\."));
        Path entitySrcDir = Paths.get(generatedSourcesDest, targetEntityPackage.split("\\."));

        // Fix any null list parameters
        if (ddlSearchPaths == null) {
            ddlSearchPaths = List.of(
                Paths.get(project.getBasedir().getAbsolutePath(), "src", "main", "resources").toString()
            );
        }

        if (entityTypeAnnotations == null) {
            entityTypeAnnotations = List.of();
        }

        if (dtoTypeAnnotations == null) {
            dtoTypeAnnotations = List.of();
        }

        if (fileFilters == null) {
            fileFilters = List.of(".*\\.sql");
        }

        // Build the DDL from SQL files
        DDL ddl = buildDDL(ddlSearchPaths);


        // Generate objects from DDL
        if (generateDtos) {
            JCodeModel dtoCodeModel = new JCodeModel();
            generateDtos(dtoCodeModel, ddl);
            JCMWriter dtoCodeModelWriter = new JCMWriter(dtoCodeModel);
            try {
                dtoCodeModelWriter.build(dtoSrcDir.toFile());
            } catch (IOException e) {
                log.error("Failed to write DTO source files to {}:", dtoSrcDir, e);
                failOnError(e);
            }
        }

        if (generateEntities) {
            JCodeModel entityCodeModel = new JCodeModel();
            generateEntities(entityCodeModel, ddl);
            JCMWriter entityCodeModelWriter = new JCMWriter(entityCodeModel);
            try {
                entityCodeModelWriter.build(entitySrcDir.toFile());
            } catch (IOException e) {
                log.error("Failed to write Entity source files to {}:", entitySrcDir, e);
                failOnError(e);
            }
        }

    }

    DDL buildDDL(List<String> ddlSearchPaths) {
        return ddlSearchPaths.stream()
            .map(Path::of)
            .filter(this::directoryPredicate)
            .flatMap(this::readSearchPaths)
            .map(Path::toFile)
            .filter(File::isFile)
            .filter(this::filterFiles)
            .map(this::getFileReader)
            .filter(Objects::nonNull)
            .map(BufferedReader::new)
            .map(reader -> reader.lines().collect(Collectors.joining()))
            .map(this::getStatements)
            .filter(Objects::nonNull)
            .map(Statements::getStatements)
            .flatMap(List::stream)
            .filter(this::filterStatements)
            .collect(new CollectorDDL());
    }

    private boolean filterStatements(Statement statement) {
        boolean b = false;
        if (includeTables) {
            b = statement instanceof CreateTable;
            b |= statement instanceof Alter;
        }
//                if (includeViews) {
//                    b |= statement instanceof CreateView;
//                    b |= statement instanceof AlterView;
//                }

        return b;
    }

    @Nullable
    private Statements getStatements(String sqls) {
        try {
            return CCJSqlParserUtil.parseStatements(sqls);
        } catch (JSQLParserException e) {
            failOnError(e);
            return null;
        }
    }

    @Nullable
    private FileReader getFileReader(File file) {
        try {
            return new FileReader(file);
        } catch (FileNotFoundException e) {
            log.error("Failed to open file {}:", file, e);
            failOnError(e);
        }
        return null;
    }

    private boolean filterFiles(File f) {
        for (String filter : fileFilters) {
            if (f.getName().matches(filter)) {
                return true;
            }
        }

        return false;
    }

    @NotNull
    private Stream<? extends Path> readSearchPaths(@NotNull Path p) {
        try {
            return Files.walk(p);
        } catch (IOException e) {
            log.error("Failed to read directory {}:", p, e);
            failOnError(e);
        }
        return Stream.empty();
    }

    private boolean directoryPredicate(@NotNull Path p) {
        if (p.toFile().isDirectory()) {
            return true;
        }
        log.warn("{} is not a directory! Skipping.", p);
        failOnError("Could not read directory " + p);
        return false;
    }

    private void failOnError(String message) {
        if (failOnError) {
            throw new RuntimeException(message);
        }
    }

    private void failOnError(Throwable t) {
        if (failOnError) {
            throw new RuntimeException(t);
        }
    }

    void generateDtos(JCodeModel codeModel, DDL ddl) {
        ddl.getTableToSchemaMap()
            .forEach((schema, tableList) -> {
                final JPackage jp = getJPackage(codeModel, targetDtoPackage, schema);

                tableList.forEach(table -> generateDtoClassForTable(ddl, table, jp));
            });
    }

    void generateEntities(JCodeModel codeModel, DDL ddl) {
        ddl.getTableToSchemaMap()
            .forEach((schema, tableList) -> {
                final JPackage jp = getJPackage(codeModel, targetEntityPackage, schema);

                tableList.forEach(table -> generateEntityClassForTable(ddl, schema, table, jp, false));
            });
    }

    @NotNull
    private JPackage getJPackage(JCodeModel codeModel, String targetBasePackage, String schema) {
        if (separatePackageBySchema) {
            return codeModel._package(targetBasePackage).subPackage(schema.toLowerCase());
        } else {
            return codeModel._package(targetBasePackage);
        }
    }

    private void generateDtoClassForTable(DDL ddl, net.sf.jsqlparser.schema.Table table, JPackage jp) {
        try {
            JDefinedClass definedClass = jp._class(
                capitalize(
                    convertCase(dtoTypePrefix + table.getName() + dtoTypeSuffix)
                        .replace(" ", "")
                )
            );

            definedClass._implements(Serializable.class);
            definedClass.javadoc().add("DTO class generated by DDL2POJO");

            ddl.getColumnToTableMap().get(table).forEach(col -> {
                JFieldVar fieldVar = definedClass.field(
                    JMod.PRIVATE,
                    TypeMapping.getTypesMap().get(col.getColDataType().toString()),
                    uncapitalize(
                        convertCase(col.getColumnName())
                            .replace(" ", "")
                    )
                );

                JGetter getter = new JGetter(
                    definedClass,
                    JMod.PUBLIC,
                    fieldVar.type(),
                    "get" + capitalize(fieldVar.name()),
                    fieldVar
                );

                JSetter setter = new JSetter(
                    definedClass,
                    JMod.PUBLIC,
                    fieldVar.type(),
                    "set" + capitalize(fieldVar.name()),
                    fieldVar
                );
            });

            JHashCodeMethod hashCodeMethod = new JHashCodeMethod(definedClass, JMod.PUBLIC);
            JToStringMethod toStringMethod = new JToStringMethod(definedClass, JMod.PUBLIC);
            JEqualsMethod equalsMethod = new JEqualsMethod(definedClass, JMod.PUBLIC);

        } catch (JCodeModelException e) {
            throw new RuntimeException(e);
        }
    }

    private JDefinedClass generateEntityClassForTable(DDL ddl, String schema, Table table, JPackage jp, boolean isPKTable) {
        try {
            // Define class
            JDefinedClass definedClass = jp._class(
                capitalize(
                    convertCase(entityTypePrefix + table.getName() + entityTypeSuffix)
                        .replace(" ", "")
                )
            );

            definedClass.javadoc().add("Entity class generated by DDL2POJO");

            definedClass.annotate(Entity.class);

            if (!isPKTable) {
                JAnnotationUse tableAnnotation = definedClass.annotate(javax.persistence.Table.class)
                    .param("tableName", table.getName())
                    .param("schema", schema);


                // Handle unique constraints
                final Hashtable<String, List<ColumnDefinition>> uks = new Hashtable<>();

                for (Constraint c : ddl.getConstraintToTableMap().get(table)) {
                    if (Objects.requireNonNull(c.getType()) == EnumConstraintType.UNIQUE) {
                        D2PUtils.putAllIntoMultivaluedMap(
                            uks,
                            c.getName(),
                            ArrayList::new,
                            c.getColumns()
                        );
                    }
                }

                if (!uks.isEmpty()) {
                    JAnnotationArrayMember annotationArrayMember = tableAnnotation.paramArray("uniqueConstraints");

                    uks.forEach((key, value) -> {
                        JAnnotationUse ja = annotationArrayMember.annotate(UniqueConstraint.class);
                        ja.param("name", key);

                        String[] colsList = value.stream()
                            .map(ColumnDefinition::getColumnName)
                            .toArray(String[]::new);

                        ja.paramArray("columnNames", colsList);
                    });
                }
            }

            // Create columns
            ddl.getColumnToTableMap().get(table).forEach(col -> {
                // Parse data type
                String dType = col.getColDataType().getDataType();
                int dTypeChopIdx = (dType.indexOf('(') == -1) ? dType.length() : dType.indexOf('(');
                Class<?> type = TypeMapping.getTypesMap().get(dType.substring(0, dTypeChopIdx));
                JFieldVar fieldVar = definedClass.field(
                    JMod.PRIVATE,
                    type,
                    uncapitalize(
                        convertCase(col.getColumnName())
                            .replace(" ", "")
                    )
                );

                JAnnotationUse colAnnotation = fieldVar.annotate(Column.class)
                    .param("name", col.getColumnName());

                // Parse data type length/scale/precision
                if (dType.contains("(")) {
                    String[] numberParams = dType
                        .substring(dTypeChopIdx + 1)
                        .replaceAll("\\s","")
                        .replace(")", "")
                        .split(",");
                    if (Number.class.isAssignableFrom(type) ||
                        List.of(int.class, float.class, double.class, long.class, short.class, byte.class)
                            .contains(type)
                    ) {
                        colAnnotation.param("scale", Integer.parseInt(numberParams[0]));
                        if (numberParams.length > 1) {
                            colAnnotation.param("precision", Integer.parseInt(numberParams[1]));
                        }
                    } else {
                        colAnnotation.param("length", Integer.parseInt(numberParams[0]));
                    }
                }

                // Generate Basic annotation
                if (Serializable.class.isAssignableFrom(type) || type.isPrimitive()) {
                    fieldVar.annotate(Basic.class);
                }

                // Generate primary key annotations
                final ArrayList<ColumnDefinition> pkCols = new ArrayList<>();

                ddl.getConstraintToTableMap().get(table)
                    .stream()
                    .filter(c -> c.getColumns().contains(col))
                    .forEach(c -> {
                        switch (c.getType()) {
                            case NON_NULL -> colAnnotation.param("nullable", false);
                            case PRIMARY_KEY -> pkCols.addAll(c.getColumns());
                        }
                    });

                if (pkCols.isEmpty()) {
                    log.warn("CRITICAL: No primary key found for table {}", table.getName());
                    failOnError("CRITICAL: No primary key found for table " + table.getName());
                } else if (pkCols.contains(col)) {
                    fieldVar.annotate(Id.class);
                }

                // Generate methods
                JGetter getter = new JGetter(
                    definedClass,
                    JMod.PUBLIC,
                    fieldVar.type(),
                    "get" + capitalize(fieldVar.name()),
                    fieldVar
                );

                JSetter setter = new JSetter(
                    definedClass,
                    JMod.PUBLIC,
                    fieldVar.type(),
                    "set" + capitalize(fieldVar.name()),
                    fieldVar
                );
            });

            if (!isPKTable) {
                if (ddl.getConstraintToTableMap().get(table).size() > 1) {
                    // Handle multi primary key constraints
                    final ArrayList<ColumnDefinition> pks = new ArrayList<>();

                    for (Constraint c : ddl.getConstraintToTableMap().get(table)) {
                        if (Objects.requireNonNull(c.getType()) == EnumConstraintType.PRIMARY_KEY) {
                            pks.addAll(c.getColumns());
                        }
                    }

                    if (pks.size() > 1) {
                        Table pkSubTable = new Table().withSchemaName(schema).withName(table.getName() + "_pk");
                        Constraint pkConstraint = new Constraint(
                            pkSubTable,
                            table.getName() + "PK",
                            pks,
                            EnumConstraintType.PRIMARY_KEY
                        );

                        DDL tempDDL = new DDL(
                            Map.of(schema, List.of(pkSubTable)),
                            Map.of(pkSubTable, pks),
                            Map.of(pkSubTable, List.of(pkConstraint))
                        );

                        JDefinedClass pkEntityClass = generateEntityClassForTable(tempDDL, schema, pkSubTable, jp, true);

                        definedClass.annotate(IdClass.class).param("value", pkEntityClass);
                    }
                }
            }

            JHashCodeMethod hashCodeMethod = new JHashCodeMethod(definedClass, JMod.PUBLIC);
            JToStringMethod toStringMethod = new JToStringMethod(definedClass, JMod.PUBLIC);
            JEqualsMethod equalsMethod = new JEqualsMethod(definedClass, JMod.PUBLIC);

            return definedClass;
        } catch (JCodeModelException e) {
            throw new RuntimeException(e);
        }
    }
}
