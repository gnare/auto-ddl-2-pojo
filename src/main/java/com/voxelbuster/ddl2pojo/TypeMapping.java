/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.ddl2pojo;

import lombok.Getter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class TypeMapping {

    @Getter
    private static final HashMap<String, Class<?>> typesMap = new HashMap<>();

    static {
        typesMap.put("char", String.class);
        typesMap.put("nchar", String.class);
        typesMap.put("nvarchar", String.class);
        typesMap.put("nvarchar2", String.class);
        typesMap.put("text", String.class);
        typesMap.put("varchar", String.class);
        typesMap.put("varchar2", String.class);
        typesMap.put("clob", String.class);
        typesMap.put("tinytext", String.class);
        typesMap.put("longtext", String.class);
        typesMap.put("mediumtext", String.class);
        typesMap.put("xml", String.class);
        typesMap.put("json", String.class);
        typesMap.put("ascii", String.class);
        typesMap.put("memo", String.class);
        typesMap.put("enum", String.class);
        typesMap.put("ntext", String.class);

        typesMap.put("bit", Integer.class);
        typesMap.put("byte", Integer.class);
        typesMap.put("tinyint", Integer.class);
        typesMap.put("smallint", Integer.class);
        typesMap.put("mediumint", Integer.class);
        typesMap.put("serial", Integer.class);
        typesMap.put("integer", Integer.class);
        typesMap.put("int", Integer.class);
        typesMap.put("year", Integer.class);
        typesMap.put("counter", Integer.class);

        typesMap.put("long", Long.class);
        typesMap.put("bigint", BigInteger.class);
        typesMap.put("autonumber", BigInteger.class);
        typesMap.put("bigserial", BigInteger.class);
        typesMap.put("varint", BigInteger.class);

        typesMap.put("float", Float.class);
        typesMap.put("smallmoney", Float.class);
        typesMap.put("single", Float.class);

        typesMap.put("double", Double.class);
        typesMap.put("double precision", Double.class);
        typesMap.put("real", Double.class);

        typesMap.put("decimal", BigDecimal.class);
        typesMap.put("bigdecimal", BigDecimal.class);
        typesMap.put("dec", BigDecimal.class);
        typesMap.put("currency", BigDecimal.class);
        typesMap.put("money", BigDecimal.class);
        typesMap.put("numeric", BigDecimal.class);
        typesMap.put("number", BigDecimal.class);

        typesMap.put("binary", ByteBuffer.class);
        typesMap.put("bytes", ByteBuffer.class);
        typesMap.put("tinyblob", ByteBuffer.class);
        typesMap.put("blob", ByteBuffer.class);
        typesMap.put("mediumblob", ByteBuffer.class);
        typesMap.put("longblob", ByteBuffer.class);
        typesMap.put("varbinary", ByteBuffer.class);
        typesMap.put("image", ByteBuffer.class);
        typesMap.put("bfile", ByteBuffer.class);
        typesMap.put("raw", ByteBuffer.class);
        typesMap.put("long raw", ByteBuffer.class);

        typesMap.put("timestamp", LocalDateTime.class);
        typesMap.put("smalldatetime", LocalDateTime.class);
        typesMap.put("datetime", LocalDateTime.class);
        typesMap.put("datetime2", LocalDateTime.class);
        typesMap.put("datetimeoffset", LocalDateTime.class);

        typesMap.put("date", LocalDate.class);

        typesMap.put("time", LocalTime.class);

        typesMap.put("uuid", UUID.class);
        typesMap.put("timeuuid", UUID.class);
        typesMap.put("uniqueidentifier", UUID.class);
        typesMap.put("guid", UUID.class);

        typesMap.put("bool", Boolean.class);
        typesMap.put("boolean", Boolean.class);
        typesMap.put("yesno", Boolean.class);

        typesMap.put("set", List.class);
        typesMap.put("array", List.class);
        typesMap.put("list", List.class);
    }
}
