/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.ddl2pojo.generators;

import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JMod;
import com.helger.jcodemodel.JVar;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;

public class JToStringMethod extends JMethod {
    public JToStringMethod(
        @NotNull JDefinedClass jDefinedClass,
        int i
    ) {
        super(jDefinedClass, i, jDefinedClass.owner().ref(String.class), "toString");

        jDefinedClass.methods().add(this);

        annotate(owner().ref(Override.class));

        JVar stringBuilderVar = body().decl(JMod.NONE,
            owner().ref(StringBuilder.class),
            "sb",
            JExpr._new(owner().ref(StringBuilder.class)));

        body().add(stringBuilderVar.invoke("append").arg("{"));
        body().add(stringBuilderVar.invoke("append").arg(JExpr.lit(jDefinedClass.name())));
        body().add(stringBuilderVar.invoke("append").arg(":{"));

        Iterator<JFieldVar> varIterator = jDefinedClass.fields().values().iterator();

        while (varIterator.hasNext()) {
            JFieldVar fieldVar = varIterator.next();
            body().add(stringBuilderVar.invoke("append").arg(JExpr.lit(fieldVar.name())));
            body().add(stringBuilderVar.invoke("append").arg(JExpr.lit(":")));
            body().add(stringBuilderVar.invoke("append").arg(fieldVar));

                if (varIterator.hasNext()) {
                    body().add(stringBuilderVar.invoke("append").arg(JExpr.lit(",")));
                }
        }

        body().add(stringBuilderVar.invoke("append").arg(JExpr.lit("}}")));

        this.body()._return(stringBuilderVar.invoke("toString"));
    }

}
