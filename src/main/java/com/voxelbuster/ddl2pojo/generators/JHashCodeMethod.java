/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.ddl2pojo.generators;

import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JInvocation;
import com.helger.jcodemodel.JMethod;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class JHashCodeMethod extends JMethod {
    public JHashCodeMethod(@NotNull JDefinedClass jDefinedClass, int nMods) {
        super(jDefinedClass, nMods, jDefinedClass.owner().ref("int"), "hashCode");

        jDefinedClass.methods().add(this);

        annotate(owner().ref(Override.class));

        JInvocation invocation = owner().ref(Objects.class).staticInvoke("hash");

        if (jDefinedClass.fields().isEmpty()) {
            this.body()._return(JExpr._super().invoke("hashCode"));
            return;
        }

        for (JFieldVar fieldVar : jDefinedClass.fields().values()) {
            invocation.arg(fieldVar);
        }

        this.body()._return(invocation);
    }
}
