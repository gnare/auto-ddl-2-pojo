/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.ddl2pojo.generators;

import com.helger.jcodemodel.AbstractJType;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JMethod;
import org.jetbrains.annotations.NotNull;

public class JGetter extends JMethod {
    public JGetter(@NotNull JDefinedClass jDefinedClass, int i, @NotNull AbstractJType abstractJType, @NotNull String s, JFieldVar field) {
        super(jDefinedClass, i, abstractJType, s);

        jDefinedClass.methods().add(this);

        this.body()._return(field);
        this.javadoc().addReturn().add(field.name());
    }
}
