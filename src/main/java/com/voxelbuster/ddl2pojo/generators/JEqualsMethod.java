/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.ddl2pojo.generators;

import com.helger.jcodemodel.IJExpression;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JFieldVar;
import com.helger.jcodemodel.JMethod;
import com.helger.jcodemodel.JMod;
import com.helger.jcodemodel.JReturn;
import com.helger.jcodemodel.JVar;
import org.jetbrains.annotations.NotNull;

public class JEqualsMethod extends JMethod {
    public JEqualsMethod(@NotNull JDefinedClass jDefinedClass, int nMods) {
        super(jDefinedClass, nMods, jDefinedClass.owner().ref("boolean"), "equals");

        JVar other = this.param(Object.class, "other");

        jDefinedClass.methods().add(this);

        annotate(owner().ref(Override.class));

        this.body()._if(JExpr._this().eq(other),
            this.body()._return(JExpr.lit(true)));
        this.body()._if(
            JExpr._this()
                .eq(JExpr._null())
                .cor(other.invoke("getClass").ne(JExpr._this().invoke("getClass"))),
            new JReturn(JExpr.lit(false)));

        if (jDefinedClass.fields().isEmpty()) {
            this.body()._return(JExpr.lit(true));
            return;
        }

        JVar that = this.body().decl(JMod.NONE,
            jDefinedClass,
            "that",
            other.castTo(jDefinedClass));

        IJExpression expr = JExpr.lit(true);

        for (JFieldVar fieldVar : jDefinedClass.fields().values()) {
            expr = expr.cand(fieldVar.eq(that.ref(fieldVar.name())));
        }

        this.body()._return(expr);
    }
}
