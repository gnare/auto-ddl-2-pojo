/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.ddl2pojo;

import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.alter.Alter;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.create.view.AlterView;
import net.sf.jsqlparser.statement.create.view.CreateView;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static com.voxelbuster.ddl2pojo.EnumConstraintType.DEFAULT;
import static com.voxelbuster.ddl2pojo.EnumConstraintType.FOREIGN_KEY;
import static com.voxelbuster.ddl2pojo.EnumConstraintType.NON_NULL;
import static com.voxelbuster.ddl2pojo.EnumConstraintType.PRIMARY_KEY;
import static com.voxelbuster.ddl2pojo.EnumConstraintType.UNIQUE;

public class CollectorDDL implements Collector<Statement, DDL, DDL> {
    private static final Supplier<List<Table>> tableListSupplier = ArrayList::new;
    private static final Supplier<List<ColumnDefinition>> colListSupplier = ArrayList::new;
    private static final Supplier<List<Constraint>> constraintListSupplier = ArrayList::new;

    @Override
    public Supplier<DDL> supplier() {
        return () -> new DDL(new Hashtable<>(), new Hashtable<>(), new Hashtable<>());
    }

    @Override
    public BiConsumer<DDL, Statement> accumulator() {
        return (ddl, statement) -> {
            if (statement instanceof CreateTable ct) {
                Table t = ct.getTable();
                D2PUtils.putIntoMultivaluedMap(ddl.getTableToSchemaMap(), t.getSchemaName(), tableListSupplier, t);
                if (ct.getColumnDefinitions() == null) {
                    return;
                }

                ct.getColumnDefinitions().forEach(cd -> {
                    if (cd.getColumnSpecs() == null) {
                        return;
                    }

                    String spec = cd.getColumnSpecs()
                        .stream()
                        .map(String::toLowerCase)
                        .collect(Collectors.joining(" "));

                    if (spec.contains("not null")) {
                        Constraint c =
                            new Constraint(t, cd.getColumnName() + "_non_null", List.of(cd), NON_NULL);
                        D2PUtils.putIntoMultivaluedMap(
                            ddl.getConstraintToTableMap(), t, constraintListSupplier, c);
                    }

                    if (spec.contains("primary key")) {
                        Constraint c =
                            new Constraint(t, t.getName() + "_pk", List.of(cd), PRIMARY_KEY);
                        D2PUtils.putIntoMultivaluedMap(
                            ddl.getConstraintToTableMap(), t, constraintListSupplier, c);
                    }

                    if (spec.contains("unique")) {
                        Constraint c =
                            new Constraint(t, cd.getColumnName() + "_uk", List.of(cd), UNIQUE);
                        D2PUtils.putIntoMultivaluedMap(
                            ddl.getConstraintToTableMap(), t, constraintListSupplier, c);
                    }

                    if (spec.contains("default")) {
                        Constraint c =
                            new Constraint(t, cd.getColumnName() + "_def", List.of(cd), DEFAULT);
                        D2PUtils.putIntoMultivaluedMap(
                            ddl.getConstraintToTableMap(), t, constraintListSupplier, c);
                    }

                    D2PUtils.putIntoMultivaluedMap(ddl.getColumnToTableMap(), t, colListSupplier, cd);
                });

            } else if (statement instanceof CreateView cv) {
                Table t = cv.getView();
                D2PUtils.putIntoMultivaluedMap(ddl.getTableToSchemaMap(), t.getSchemaName(), tableListSupplier, t);
                if (cv.getColumnNames() != null) {
                    cv.getColumnNames().forEach(s -> {
                            ColumnDefinition cd = new ColumnDefinition().withColumnName(s);
                            D2PUtils.putIntoMultivaluedMap(ddl.getColumnToTableMap(), t, colListSupplier, cd);
                        }
                    );
                }
            } else if (statement instanceof Alter a) {
                Table t = a.getTable();
                if (a.getAlterExpressions() != null) {
                    a.getAlterExpressions().forEach(exp -> {
                        if (!exp.getUkColumns().isEmpty()) {
                            List<ColumnDefinition> cds = exp.getUkColumns()
                                .stream()
                                .map(s -> new ColumnDefinition().withColumnName(s))
                                .toList();
                            String cName = exp.getUkName();
                            Constraint c = new Constraint(t, cName, cds, UNIQUE);
                            D2PUtils.putIntoMultivaluedMap(
                                ddl.getConstraintToTableMap(), t, constraintListSupplier, c);
                        }
                        if (!exp.getPkColumns().isEmpty()) {
                            List<ColumnDefinition> cds = exp.getPkColumns()
                                .stream()
                                .map(s -> new ColumnDefinition().withColumnName(s))
                                .toList();
                            String cName = t.getName() + "_pk";
                            Constraint c = new Constraint(t, cName, cds, PRIMARY_KEY);
                            D2PUtils.putIntoMultivaluedMap(
                                ddl.getConstraintToTableMap(), t, constraintListSupplier, c);
                        }
                        if (!exp.getFkColumns().isEmpty()) {
                            List<ColumnDefinition> cds = exp.getFkColumns()
                                .stream()
                                .map(s -> new ColumnDefinition().withColumnName(s))
                                .toList();
                            String cName = t.getName() + "_" + exp.getFkSourceTable() + "_fk";
                            Constraint c = new Constraint(t, cName, cds, FOREIGN_KEY);
                            D2PUtils.putIntoMultivaluedMap(
                                ddl.getConstraintToTableMap(), t, constraintListSupplier, c);
                        }
                    });
                }
            } else if (statement instanceof AlterView av) {
                Table t = av.getView();
            }
        };
    }

    @Override
    public BinaryOperator<DDL> combiner() {
        return (ddl1, ddl2) -> {
            ddl1.getTableToSchemaMap().putAll(ddl2.getTableToSchemaMap());
            ddl1.getColumnToTableMap().putAll(ddl2.getColumnToTableMap());
            ddl1.getConstraintToTableMap().putAll(ddl2.getConstraintToTableMap());
            return ddl1;
        };
    }

    @Override
    public Function<DDL, DDL> finisher() {
        return ddl -> ddl;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Set.of();
    }
}
