/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.ddl2pojo;

import lombok.NonNull;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class D2PUtils {
    public static <K, V, C extends Collection<V>> boolean putIntoMultivaluedMap(
        @NonNull Map<K, C> map,
        @NonNull K key,
        @NonNull Supplier<C> collectionSupplier,
        V value
    ) {

        C coll = map.get(key);
        if (coll == null) {
            coll = collectionSupplier.get();
            coll.add(value);
            map.put(key, coll);
            return false;
        }

        coll.add(value);
        return true;
    }

    public static <K, V, C extends Collection<V>> boolean putAllIntoMultivaluedMap(
        @NonNull Map<K, C> map,
        @NonNull K key,
        @NonNull Supplier<C> collectionSupplier,
        Collection<V> value
    ) {

        C coll = map.get(key);
        if (coll == null) {
            coll = collectionSupplier.get();
            coll.addAll(value);
            map.put(key, coll);
            return false;
        }

        coll.addAll(value);
        return true;
    }

    @Contract(pure = true)
    public static @NotNull String convertCase(@NotNull String snakeOrCamelCase) {
        // Split snake_case
        return Arrays.stream(snakeOrCamelCase.split("[_\\s]"))
            // Split camelCase
            .map(input -> input.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])"))
            .flatMap(Arrays::stream)
            .map(String::toLowerCase)
            // Capitalize first letter of each
            .map(input -> input.substring(0, 1).toUpperCase() + input.substring(1))
            // Stick them all together with spaces
            .collect(Collectors.joining(" ")).trim();
    }

    @Contract(pure = true, value = "_ -> new")
    public static String capitalize(@NotNull String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    @Contract(pure = true, value = "_ -> new")
    public static String uncapitalize(@NotNull String s) {
        return s.substring(0, 1).toLowerCase() + s.substring(1);
    }
}
