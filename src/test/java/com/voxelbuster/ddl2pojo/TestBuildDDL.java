/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.ddl2pojo;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
public class TestBuildDDL {

    private DDL2PojoMojo mojo;

    @BeforeEach
    public void setup() throws IllegalAccessException, NoSuchFieldException {
        this.mojo = new DDL2PojoMojo();

        Field separatePackageBySchema = mojo.getClass().getDeclaredField("fileFilters");
        separatePackageBySchema.trySetAccessible();
        separatePackageBySchema.set(mojo, List.of(".*\\.sql"));

        Field includeTables = mojo.getClass().getDeclaredField("includeTables");
        includeTables.trySetAccessible();
        includeTables.set(mojo, true);
    }

    @Test
    public void testBuildDDL() throws NoSuchFieldException, IllegalAccessException {
        DDL ddl = mojo.buildDDL(List.of("src/test/resources/testSql1"));

        log.info(ddl.toString());
    }

    @Test
    public void testEmpty() {
        DDL ddl = mojo.buildDDL(List.of("src/test/resources/testSqlEmpty"));

        log.info(ddl.toString());

        assertTrue(ddl.getColumnToTableMap().isEmpty());
        assertTrue(ddl.getTableToSchemaMap().isEmpty());
        assertTrue(ddl.getConstraintToTableMap().isEmpty());
    }
}