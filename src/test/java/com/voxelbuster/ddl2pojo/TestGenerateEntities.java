/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.ddl2pojo;

import com.helger.jcodemodel.JCodeModel;
import com.helger.jcodemodel.writer.OutputStreamCodeWriter;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.create.table.ColDataType;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

@Slf4j
public class TestGenerateEntities {
    @Test
    public void testGenEntities() throws NoSuchFieldException, IllegalAccessException, IOException {
        Table table1 = new Table().withName("table1").withSchemaName("test");

        ColumnDefinition idCol = new ColumnDefinition().withColumnName("id").withColDataType(new ColDataType().withDataType("varchar2(20)"));

        DDL2PojoMojo mojo = new DDL2PojoMojo();

        DDL ddl = new DDL(Map.of("test", List.of(table1)),
            Map.of(table1, List.of(idCol)),
            Map.of(table1, List.of()));

        JCodeModel model = new JCodeModel();

        Field separatePackageBySchema = mojo.getClass().getDeclaredField("separatePackageBySchema");
        separatePackageBySchema.trySetAccessible();
        separatePackageBySchema.set(mojo, true);

        Field targetEntityPackage = mojo.getClass().getDeclaredField("targetEntityPackage");
        targetEntityPackage.trySetAccessible();
        targetEntityPackage.set(mojo, "entity");

        Field prefix = mojo.getClass().getDeclaredField("entityTypePrefix");
        prefix.trySetAccessible();
        prefix.set(mojo, "");

        Field suffix = mojo.getClass().getDeclaredField("entityTypeSuffix");
        suffix.trySetAccessible();
        suffix.set(mojo, "Entity");

        mojo.generateEntities(model, ddl);

        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        model.build(new OutputStreamCodeWriter(ostream, Charset.defaultCharset()));

        log.info(ostream.toString());
    }

    @Test
    public void testGenEntitiesWithConstraints() throws NoSuchFieldException, IllegalAccessException, IOException {
        Table table1 = new Table().withName("table1").withSchemaName("test");

        ColumnDefinition idCol = new ColumnDefinition().withColumnName("id").withColDataType(new ColDataType().withDataType("varchar2(20)"));
        ColumnDefinition valCol = new ColumnDefinition().withColumnName("val").withColDataType(new ColDataType().withDataType("float(10, 2)"));
        ColumnDefinition numCol = new ColumnDefinition().withColumnName("num").withColDataType(new ColDataType().withDataType("int(8)"));

        DDL2PojoMojo mojo = new DDL2PojoMojo();

        DDL ddl = new DDL(Map.of("test", List.of(table1)),
            Map.of(table1, List.of(idCol, valCol, numCol)),
            Map.of(table1, List.of(
                new Constraint(table1, "uk", List.of(idCol), EnumConstraintType.UNIQUE),
                new Constraint(table1, "nn", List.of(idCol), EnumConstraintType.NON_NULL)
            )));

        JCodeModel model = new JCodeModel();

        Field separatePackageBySchema = mojo.getClass().getDeclaredField("separatePackageBySchema");
        separatePackageBySchema.trySetAccessible();
        separatePackageBySchema.set(mojo, true);

        Field targetEntityPackage = mojo.getClass().getDeclaredField("targetEntityPackage");
        targetEntityPackage.trySetAccessible();
        targetEntityPackage.set(mojo, "entity");

        Field prefix = mojo.getClass().getDeclaredField("entityTypePrefix");
        prefix.trySetAccessible();
        prefix.set(mojo, "");

        Field suffix = mojo.getClass().getDeclaredField("entityTypeSuffix");
        suffix.trySetAccessible();
        suffix.set(mojo, "Entity");

        mojo.generateEntities(model, ddl);

        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        model.build(new OutputStreamCodeWriter(ostream, Charset.defaultCharset()));

        log.info(ostream.toString());
    }

    @Test
    public void testGenEntitiesWithCompoundPK() throws NoSuchFieldException, IllegalAccessException, IOException {
        Table table1 = new Table().withName("table1").withSchemaName("test");

        ColumnDefinition idCol = new ColumnDefinition().withColumnName("id").withColDataType(new ColDataType().withDataType("varchar2(20)"));
        ColumnDefinition valCol = new ColumnDefinition().withColumnName("val").withColDataType(new ColDataType().withDataType("float(10, 2)"));
        ColumnDefinition numCol = new ColumnDefinition().withColumnName("num").withColDataType(new ColDataType().withDataType("int(8)"));

        DDL2PojoMojo mojo = new DDL2PojoMojo();

        DDL ddl = new DDL(Map.of("test", List.of(table1)),
            Map.of(table1, List.of(idCol, valCol, numCol)),
            Map.of(table1, List.of(
                new Constraint(table1, "pk", List.of(idCol, valCol), EnumConstraintType.PRIMARY_KEY),
                new Constraint(table1, "nn", List.of(numCol), EnumConstraintType.NON_NULL)
            )));

        JCodeModel model = new JCodeModel();

        Field separatePackageBySchema = mojo.getClass().getDeclaredField("separatePackageBySchema");
        separatePackageBySchema.trySetAccessible();
        separatePackageBySchema.set(mojo, true);

        Field targetEntityPackage = mojo.getClass().getDeclaredField("targetEntityPackage");
        targetEntityPackage.trySetAccessible();
        targetEntityPackage.set(mojo, "entity");

        Field prefix = mojo.getClass().getDeclaredField("entityTypePrefix");
        prefix.trySetAccessible();
        prefix.set(mojo, "");

        Field suffix = mojo.getClass().getDeclaredField("entityTypeSuffix");
        suffix.trySetAccessible();
        suffix.set(mojo, "Entity");

        mojo.generateEntities(model, ddl);

        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        model.build(new OutputStreamCodeWriter(ostream, Charset.defaultCharset()));

        log.info(ostream.toString());
    }
}
