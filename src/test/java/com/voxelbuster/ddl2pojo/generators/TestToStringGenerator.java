/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.ddl2pojo.generators;import com.helger.jcodemodel.JCodeModel;
import com.helger.jcodemodel.JCodeModelException;
import com.helger.jcodemodel.JDefinedClass;
import com.helger.jcodemodel.JExpr;
import com.helger.jcodemodel.JMod;
import com.helger.jcodemodel.JPackage;
import com.helger.jcodemodel.writer.OutputStreamCodeWriter;
import com.voxelbuster.ddl2pojo.generators.JToStringMethod;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

@Slf4j
public class TestToStringGenerator {
    @Test
    public void testEmptyClass() throws JCodeModelException, IOException {
        JCodeModel model = new JCodeModel();
        JPackage jp = model._package("test").subPackage("pack");
        JDefinedClass jc = jp._class("EmptyToStringClass");
        JToStringMethod generated = new JToStringMethod(jc, JMod.PUBLIC);

        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        model.build(new OutputStreamCodeWriter(ostream, Charset.defaultCharset()));

        log.info(ostream.toString());
    }

    @Test
    public void testMultiFieldClass() throws JCodeModelException, IOException {
        JCodeModel model = new JCodeModel();
        JPackage jp = model._package("test").subPackage("pack");
        JDefinedClass jc = jp._class("MultiToStringClass");

        jc.field(JMod.PRIVATE, String.class, "a", JExpr.lit("Hello World"));
        jc.field(JMod.PRIVATE, int.class, "b", JExpr.lit(72));
        jc.field(JMod.PRIVATE_FINAL, boolean.class, "foo", JExpr.lit(true));

        JToStringMethod generated = new JToStringMethod(jc, JMod.PUBLIC);

        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

        model.build(new OutputStreamCodeWriter(ostream, Charset.defaultCharset()));

        log.info(ostream.toString());
    }
}
